<?php
/**
 * Ticker, the price, volume, market cap ...
 */

namespace AzureSpring\CoinMarketCap\Model;

/**
 * Ticker
 */
class Ticker
{
    private $obj;

    private $currency;


    /**
     * Construct
     *
     * @param mixed       $obj
     * @param string|null $currency alternative currency
     */
    public function __construct($obj, ?string $currency = null)
    {
        $this->obj = $obj;
        $this->currency = $currency;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->obj->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->obj->name;
    }

    /**
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->obj->symbol;
    }

    /**
     * @return int
     */
    public function getRank(): int
    {
        return (int) $this->obj->rank;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return (float) $this->obj->price_usd;
    }

    /**
     * @return float
     */
    public function getPriceAlt(): float
    {
        if (!$this->currency) {
            throw new \LogicException();
        }

        return (float) $this->obj->{'price_'.strtolower($this->currency)};
    }

    /**
     * @return float
     */
    public function getPriceBTC(): float
    {
        return (float) $this->obj->price_btc;
    }

    /**
     * @return float
     */
    public function get24hVolume(): float
    {
        return (float) $this->obj->{'24h_volume_usd'};
    }

    /**
     * @return float
     */
    public function get24hVolumeAlt(): float
    {
        if (!$this->currency) {
            throw new \LogicException();
        }

        return (float) $this->obj->{'24h_volume_'.strtolower($this->currency)};
    }

    /**
     * @return float
     */
    public function getMarketCap(): float
    {
        return (float) $this->obj->market_cap_usd;
    }

    /**
     * @return float
     */
    public function getMarketCapAlt(): float
    {
        if (!$this->currency) {
            throw new \LogicException();
        }

        return (float) $this->obj->{'market_cap_'.strtolower($this->currency)};
    }

    /**
     * @return float
     */
    public function getAvailableSupply(): float
    {
        return (float) $this->obj->available_supply;
    }

    /**
     * @return float
     */
    public function getTotalSupply(): float
    {
        return (float) $this->obj->total_supply;
    }

    /**
     * @return float
     */
    public function getMaxSupply(): float
    {
        return (float) $this->obj->max_supply;
    }

    /**
     * @return float
     */
    public function getPercentChange1h(): float
    {
        return (float) $this->obj->percent_change_1h;
    }

    /**
     * @return float
     */
    public function getPercentChange24h(): float
    {
        return (float) $this->obj->percent_change_24h;
    }

    /**
     * @return float
     */
    public function getPercentChange7d(): float
    {
        return (float) $this->obj->percent_change_7d;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdated(): \DateTime
    {
        return (new \DateTime())->setTimestamp($this->obj->last_updated);
    }
}
