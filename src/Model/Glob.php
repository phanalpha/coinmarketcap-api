<?php
/**
 * Global Data
 */

namespace AzureSpring\CoinMarketCap\Model;

/**
 * Glob
 */
class Glob
{
    private $obj;

    private $currency;


    /**
     * Construct
     *
     * @param mixed       $obj
     * @param string|null $currency
     */
    public function __construct($obj, ?string $currency = null)
    {
        $this->obj = $obj;
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getTotalMarketCap(): float
    {
        return (float) $this->obj->total_market_cap_usd;
    }

    /**
     * @return float
     */
    public function getTotalMarketCapAlt(): float
    {
        if (!$this->currency) {
            throw new \LogicException();
        }

        return (float) $this->obj->{'total_market_cap_'.strtolower($this->currency)};
    }

    /**
     * @return float
     */
    public function getTotal24hVolume(): float
    {
        return (float) $this->obj->total_24h_volume_usd;
    }

    /**
     * @return float
     */
    public function getTotal24hVolumeAlt(): float
    {
        if (!$this->currency) {
            throw new \LogicException();
        }

        return (float) $this->obj->{'total_24h_volume_'.strtolower($this->currency)};
    }

    /**
     * @return float
     */
    public function getBitcoinPercentageOfMarketCap(): float
    {
        return (float) $this->obj->bitcoin_percentage_of_market_cap;
    }

    /**
     * Coins
     *
     * @return int
     */
    public function getActiveCurrencies(): int
    {
        return (int) $this->obj->active_currencies;
    }

    /**
     * Tokens
     *
     * @return int
     */
    public function getActiveAssets(): int
    {
        return (int) $this->obj->active_assets;
    }

    /**
     * @return int
     */
    public function getActiveMarkets(): int
    {
        return (int) $this->obj->active_markets;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdated(): \DateTime
    {
        return (new \DateTime())->setTimestamp($this->obj->last_updated);
    }
}
