<?php
/**
 * CoinMarketCap JSON API
 */

namespace AzureSpring\CoinMarketCap;

use AzureSpring\CoinMarketCap\Model\Glob;
use AzureSpring\CoinMarketCap\Model\Ticker;
use GuzzleHttp\Client;

/**
 * Gateway
 */
class Gateway
{
    private $guzzle;


    /**
     * Construct
     *
     * @param Client $guzzle
     */
    public function __construct(Client $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    /**
     * @param int|null    $start
     * @param int|null    $limit
     * @param string|null $convert
     *
     * @return Ticker[]
     */
    public function findAllTickers(?int $start = null, ?int $limit = null, ?string $convert = null): array
    {
        $response = $this->guzzle->get('/v1/ticker/', [
            'query' => [
                'start' => $start,
                'limit' => $limit,
                'convert' => $convert,
            ],
        ]);

        return array_map(
            function ($obj) use ($convert) {
                return new Ticker($obj, $convert);
            },
            json_decode($response->getBody())
        );
    }

    /**
     * @param string      $id
     * @param string|null $convert
     *
     * @return Ticker
     */
    public function getTicker(string $id, ?string $convert = null): Ticker
    {
        $response = $this->guzzle->get("/v1/ticker/{$id}/", [
            'query' => [
                'convert' => $convert,
            ],
        ]);

        $document = json_decode($response->getBody());
        if (is_array($document)) {
            return new Ticker(current($document), $convert);
        }

        throw new \RuntimeException($document->error);
    }

    /**
     * @param string|null $convert
     *
     * @return Glob
     */
    public function glob(?string $convert = null): Glob
    {
        $response = $this->guzzle->get('/v1/global/', [
            'query' => [
                'convert' => $convert,
            ],
        ]);

        return new Glob(json_decode($response->getBody()), $convert);
    }
}
